module.exports = function(app, passport) {

    // locally --------------------------------
        // Iniciar sesíón ===============================
        app.get('/login', function(req, res) {
            res.render('login.ejs', { message: req.flash('loginMessage') });
        });


        // process the login form
        app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/menu', //
            failureRedirect : '/login', //
            failureFlash : true // flash messages
        }));

        // SIGNUP =================================
        app.get('/signup', function(req, res) {
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        });

        // signup form
        app.post('/signup', passport.authenticate('local-signup', {
            successRedirect : '/menu', // 
            failureRedirect : '/signup', // 
            failureFlash : true // flash messages
        }));

   

    // local -----------------------------------
    app.get('/unlink/local', isLoggedIn, function(req, res) {
        var user            = req.user;
        user.local.email    = undefined;
        user.local.password = undefined;
        user.save(function(err) {
            res.redirect('/');
        });
    });
};

// route middleware 
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}
