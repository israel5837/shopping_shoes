var express = require('express');
var port = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var multer = require('multer');
var cloudinary = require("cloudinary");
var method_override = require("method-override");
var app_password = "123";
var errorhandler = require("errorhandler");



var app = express();

//Conexión cloudinary.
cloudinary.config({
	cloud_name: "djrzn6cxw",
	api_key: "682379993159167",
	api_secret: "gVguLFjCC82YT2-RdiynZzqUBEo"
});


var configDB = require('./config/database.js');

//  Conexión a Base de datos ===========================
mongoose.connect(configDB.url); // Base de Datos

require('./config/passport')(passport); // Configuración passport

app.use(morgan('dev')); //
app.use(cookieParser()); // cookies
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));

app.use(multer({
	dest: "./uploads"
}));


//Schema de Producto
var productSchema = {
	title: String,
	description: String,
	imageUrl: String,
	pricing: Number
};

var Product = mongoose.model("Product", productSchema);

app.set('view engine', 'ejs'); // ejs 


//Middlewares.

// Requerido por Passport
app.use(session({ secret: 'adjaskldjlndcsn/&lkdn' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
//======================================= Fin passport
app.set("view engine", "jade");
app.use(express.static("public"));
app.use(method_override("_method"))


//Routing
app.get("/", function(req,res,next) {
	console.log(req.session.user_id);
	res.render("index");
});

app.get("/menu", function(req,res,next) {
	Product.find(function(error, documento) {
		if (error) {
			console.log(error);
		}
		res.render("menu/index", {
			products: documento
		})
	});
});


//ACTUALIZAR PRODUCTO ==========================================
app.put("/menu/:id", function(req,res,next) {
	if (req.body.password == app_password) {
		var data = {
			title: req.body.title,
			description: req.body.description,
			pricing: req.body.pricing
		};

		if (req.files.hasOwnProperty("image_avatar")) {
			cloudinary.uploader.upload(req.files.image_avatar.path,
				function(result) {
					req
					data.imageUrl = result.url;

					Product.update({
						"_id": req.params.id
					}, data, function(product) {
						res.redirect("/menu");
					});
				}
			);
		} else {
			Product.update({
				"_id": req.params.id
			}, data, function(product) {
				res.redirect("/menu");
			});
		}


	} else {
		res.redirect("/");
	}
});


//EDITAR PRODUCTO ============================================
app.get("/menu/edit/:id", function(req, res) {
	var id_producto = req.params.id;
	console.log(id_producto);
	Product.findOne({
		"_id": id_producto
	}, function(error, producto) {
		console.log(producto);
		res.render("menu/edit", {
			product: producto
		});
	});

});

app.post("/admin", function(req, res) {
	if (req.body.password == app_password) {
		Product.find(function(error, documento) {
			if (error) {
				console.log(error);
			}
			res.render("admin/index", {
				products: documento
			})
		});
	} else {
		res.redirect("/menu");
	}
});



app.get("/admin", function(req, res) {
	res.render("admin/form");
});


// NUEVO PRODUCTO ============================================
app.post("/menu", function(req, res) {

	if (req.body.password == app_password) {

		var data = {
			title: req.body.title,
			description: req.body.description,
			imageUrl: req.body.imageUrl,
			pricing: req.body.pricing
		}

		var product = new Product(data);

		cloudinary.uploader.upload(req.files.image_avatar.path, function(result) {
			product.imageUrl = result.url;
			product.save(function(err) {
				console.log(product)
				res.render("index");
			});
		});
	} else {
		res.render("menu/new");
	}
});


//ELIMINAR PRODUCTO ============================================
app.get("/menu/new", function(req, res) {
	res.render("menu/new");
});

app.get("/menu/delete/:id", function(req, res) {
	var id = req.params.id;

	Product.findOne({
		"_id": id
	}, function(err, producto) {
		res.render("menu/delete", {
			producto: producto
		});
	});

});

app.delete("/menu/:id", function(req, res) {
	var id = req.params.id;
	if (req.body.password == app_password) {
		Product.remove({
			"_id": id
		}, function(err) {
			if (err) {
				console.log(err);
			}
			res.redirect("/menu");
		});
	} else {
		res.redirect("/menu");
	}
});


// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

// Puerto ======================================================================
app.listen(port);
console.log('Puerto:' + port);
